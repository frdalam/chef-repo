#
# Cookbook:: tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

include_recipe 'java'


directory '/opt/tomcat' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

user 'tomcat' do
  comment 'tomcat'
  home '/opt/tomcat'
  system true
  shell '/bin/false'
end

group 'tomcat' do
  action :create
end

ark 'tomcat' do
  url 'https://www-eu.apache.org/dist/tomcat/tomcat-8/v8.5.50/bin/apache-tomcat-8.5.50.tar.gz'
  home_dir '/opt/tomcat/apache-tomcat'
  prefix_root '/opt'
  owner 'tomcat'
  group 'tomcat'
end

directory '/etc/tomcat' do
  owner 'tomcat'
  group 'tomcat'
  mode '0755'
  recursive 'true'
end

template "/opt/tomcat/apache-tomcat/conf/tomcat-users.xml" do
  source 'tomcat-users.xml'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/opt/tomcat/apache-tomcat/webapps/manager/META-INF/context.xml" do
  source 'context.xml'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat.service'
  owner 'root'
  group 'root'
  mode '0755'
  notifies :enable, 'service[tomcat]', :delayed
  notifies :restart, 'service[tomcat]', :delayed
end

service 'tomcat' do
  supports restart: true
  action :enable
end


